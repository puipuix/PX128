class_name TimerData

const TIMER_COLOR := Color.LIGHT_SLATE_GRAY

var time := 0.0
var time_left := 0.0

static func score_to_time(score: int):
	return max(5.0, 1.0 / log(score * 0.006 + 1.034))

func reset(score: int):
	time = score_to_time(score)
	time_left = time

func update(delta: float):
	time_left = max(0, time_left - delta)
	
func is_ended() -> bool: return time != time_left and time_left <= 0.0

func draw(y: int, x_min: int , x_max: int, display: Image):
	if time <= 0 || x_min == x_max:
		return
	
	var center = (x_min + x_max - 1) * 0.5
	var width = x_max - x_min
	var step = 2 * time / width
	for x in range(x_min, x_max):
		var distance = abs(x - center) + 0.5
		var x_time = step * distance
		if time_left > x_time:
			display.set_pixel(x,y,TIMER_COLOR)
		else:
			var display_color = display.get_pixel(x,y)
			var weight = (time_left - x_time + step) / step
			var color = lerp(display_color, TIMER_COLOR, max(0, weight))
			display.set_pixel(x,y,color)
