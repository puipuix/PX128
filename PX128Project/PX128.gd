extends Node2D

class_name Main

const SHAPE_COLOR_VALUE := 128
const SHAPE_COLOR_RED := Color8(SHAPE_COLOR_VALUE,0,0)
const SHAPE_COLOR_GREEN := Color8(0,SHAPE_COLOR_VALUE,0)
const SHAPE_COLOR_BLUE := Color8(0,0,SHAPE_COLOR_VALUE)
const SHAPE_COLORS := [SHAPE_COLOR_RED, SHAPE_COLOR_GREEN, SHAPE_COLOR_BLUE]

const GAME_SIZE := Vector2i(8,16)
const GRID_SIZE := Vector2i(8,8)
const GRID_MAX_COLOR_VALUE := 255 + SHAPE_COLOR_VALUE / 2 # add margin


@export
var background_color := Color8(32,32,32)

var _centered_top_left : Vector2i
var _display_size : Vector2i

var _shape_square := ShapeData.new([[1,1],[1,1]])
var _shape_line := ShapeData.new([[1,1,1,1]])
var _shape_l := ShapeData.new([[1,1,1],[0,0,1]])
var _shape_s := ShapeData.new([[0,1],[1,1],[1,0]])
var _shape_t := ShapeData.new([[0,1],[1,1],[0,1]])
var _shapes := [_shape_square, _shape_line, _shape_l, _shape_s, _shape_t]

var _button_rotate := ButtonData.new(Rect2i(6,14,1,1), KEY_R)
var _button_enter := ButtonData.new(Rect2i(3,11,2,2), KEY_SPACE)
var _button_up := ButtonData.new(Rect2i(3,9,2,1), KEY_Z)
var _button_left := ButtonData.new(Rect2i(1,11,1,2), KEY_Q)
var _button_down := ButtonData.new(Rect2i(3,14,2,1), KEY_S)
var _button_right := ButtonData.new(Rect2i(6,11,1,2), KEY_D)
var _buttons := [_button_enter, _button_up, _button_left, _button_down, _button_right, _button_rotate]

var _timer := TimerData.new()
var _grid := Image.new()
var _display = Image.new()
var _texture = ImageTexture.new()

var _current_score := 0
var _current_shape : ShapeData
var _current_shape_position := Vector2i.ZERO
var _shape_lock := false

func _ready():
	DisplayServer.window_set_position(DisplayServer.window_get_position() - Vector2i(128,256))
	DisplayServer.window_set_min_size(Vector2(256,512))
	get_viewport().size_changed.connect(_on_viewport_changed)
	_grid.create(8, 8, false, Image.FORMAT_RGB8)
	_restart()
	_on_viewport_changed()

func _on_viewport_changed():
	_display_size = get_viewport_rect().size
	_centered_top_left = (_display_size - GAME_SIZE) * 0.5
	_display.create(_display_size.x, _display_size.y, false, Image.FORMAT_RGB8)
	update()
	
func _process(delta):
	update()
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
		return
	
	for button in _buttons:
		button.update(_centered_top_left, self)
		
	var movement := Vector2i.ZERO
	if _button_down.just_released:
		movement += Vector2i.DOWN
	if _button_up.just_released:
		movement += Vector2i.UP
	if _button_left.just_released:
		movement += Vector2i.LEFT
	if _button_right.just_released:
		movement += Vector2i.RIGHT
	
	if _button_rotate.just_released:
		var half = _current_shape.size / 2 # Recenter
		_current_shape.add_rotation(1)
		_current_shape_position += half - Vector2i.ONE # Recenter
	
	_timer.update(delta)
	if _timer.is_ended():
		if _can_place_shape():
			_place_and_change_shape()
		else:
			_restart()

	if not _shape_lock and _button_enter.just_released:
		if _can_place_shape():
			_place_and_change_shape()
	
	_current_shape_position += movement
	_current_shape_position.x = clamp(_current_shape_position.x, 0, GRID_SIZE.x - _current_shape.size.x)
	_current_shape_position.y = clamp(_current_shape_position.y, 0, GRID_SIZE.y - _current_shape.size.y)

func _place_and_change_shape():
	_place_shape()
	_try_clear(_current_shape_position, _current_shape_position + _current_shape.size)
	var half = _current_shape.size / 2 # Recenter
	_pick_random_shape()
	_current_shape_position += half - Vector2i.ONE # Recenter
	_timer.reset(_current_score)
	update()

func _draw():
	_display.fill(background_color)
	_display.blit_rect(_grid, Rect2i(Vector2i.ZERO, GRID_SIZE), _centered_top_left)
	_current_shape.draw(_current_shape_position, _centered_top_left, _grid, _display)

	var out_color := Ref.new()
	if _can_place_shape(out_color):
		_button_enter.custom_color = null 
	elif out_color.ref is Color:
		_button_enter.custom_color = out_color.ref

	for button in _buttons:
		button.draw(_centered_top_left, _display)
	
	_timer.draw(_centered_top_left.y + GRID_SIZE.y, _centered_top_left.x, _centered_top_left.x + GRID_SIZE.x, _display)
	_texture.set_image(_display)
	draw_texture(_texture, Vector2.ZERO)

func _restart():
	_reset_grid()
	_pick_random_shape()
	_timer.reset(0)
	_current_shape_position = (GRID_SIZE - _current_shape.size) / 2
	_current_score = 0

func _check_colors(i_start: int, i_end: int, j_start: int, j_end: int, get_pixel: Callable) -> Array:
	var i_to_clear = []
	for i in range(i_start, i_end):
		var same_color = true
		var test_color = get_pixel.call(i, 0)
		for j in range(j_start, j_end):
			if get_pixel.call(i, j) != test_color:
				same_color = false
				break
		if same_color:
			i_to_clear.append(i)
	return i_to_clear

func _reset_grid():
	_grid.fill(Color.BLACK)

func _try_clear(start: Vector2i, end: Vector2i):
	var x_to_clear = _check_colors(start.x, end.x, 0, GRID_SIZE.y, func(i,j): return _grid.get_pixel(i,j))
	var y_to_clear = _check_colors(start.y, end.y, 0, GRID_SIZE.x, func(i,j): return _grid.get_pixel(j,i))
	var add_score := 0
	var score_mult := 0
	for x in x_to_clear:
		var grid_color = _grid.get_pixel(x, 0)
		add_score += (grid_color.r8 + grid_color.g8 + grid_color.b8) / SHAPE_COLOR_VALUE
		score_mult += 1
		for y in GRID_SIZE.y:
			_grid.set_pixel(x,y,Color.BLACK)
	for y in y_to_clear:
		var grid_color = _grid.get_pixel(0, y)
		add_score += (grid_color.r8 + grid_color.g8 + grid_color.b8) / SHAPE_COLOR_VALUE
		score_mult += 1
		for x in GRID_SIZE.x:
			_grid.set_pixel(x,y,Color.BLACK)
	
	if add_score > 0:
		_current_score += add_score * score_mult
		print("Add score: ", add_score, "(x", score_mult, ") -> ", _current_score)
		print("New time: ", TimerData.score_to_time(_current_score))

func _can_place_shape(out_color: Ref = null) -> bool:
	for y in _current_shape.size.y:
		for x in _current_shape.size.x:
			if _current_shape.shape[y][x] != 0:
				var pixel_pos = _current_shape_position + Vector2i(x,y)
				var grid_color = _grid.get_pixelv(pixel_pos)
				if (grid_color.r8 + _current_shape.base_color.r8 > GRID_MAX_COLOR_VALUE
					or grid_color.g8 + _current_shape.base_color.g8 > GRID_MAX_COLOR_VALUE
					or grid_color.b8 + _current_shape.base_color.b8 > GRID_MAX_COLOR_VALUE):
					if out_color != null:
						out_color.ref = grid_color
					return false
	return true

func _place_shape():
	for y in _current_shape.size.y:
		for x in _current_shape.size.x:
			if _current_shape.shape[y][x] != 0:
				var pixel_pos = _current_shape_position + Vector2i(x,y)
				var grid_color = _grid.get_pixelv(pixel_pos)
				_grid.set_pixelv(pixel_pos, grid_color + _current_shape.base_color)

func _pick_random_shape():
	var shape = _shapes[randi_range(0, _shapes.size() - 1)]
	var color = SHAPE_COLORS[randi_range(0, SHAPE_COLORS.size() - 1)]
	_current_shape = (
		shape.copy()
		.add_color(color)
		.add_symmetry(randf() < 0.5)
		.add_rotation(randi_range(0, 3))
		)
