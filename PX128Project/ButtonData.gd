class_name ButtonData

const BUTTON_BASE_COLOR := Color.DARK_GRAY
const BUTTON_PRESSED_COLOR := Color.LIGHT_GRAY

var custom_color
var rect : Rect2i
var pressed : bool
var just_released : bool
var just_pressed : bool
var key : int

func _init(_rect : Rect2i, _key := KEY_NONE):
	rect = _rect
	key = _key
	
func draw(centered_top_left: Vector2i, display: Image):
	var color = BUTTON_BASE_COLOR
	if pressed:
		color = BUTTON_PRESSED_COLOR
	if custom_color is Color:
		color = custom_color
	var draw_rect = Rect2i(rect)
	draw_rect.position += centered_top_left
	display.fill_rect(draw_rect, color)

func update(centered_top_left: Vector2i, main: Node2D):
	var mouse_pos := Vector2i(main.get_local_mouse_position())
	var is_on = rect.has_point(mouse_pos - centered_top_left)
	var is_pressed = (is_on and Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT)) or Input.is_key_pressed(key)
	just_pressed = not pressed and is_pressed
	just_released = pressed and not is_pressed
	pressed = is_pressed
	if just_pressed or just_released:
		main.update()
