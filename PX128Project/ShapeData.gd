class_name ShapeData

const SHAPE_FADE_SPEED := 2.0
const SHAPE_FADE_SCALE := 1.5

var base_color := Color.BLACK
var size := Vector2i.ZERO
var shape := []

func _init(_shape : Array, _color := Color.BLACK):
	shape = _shape
	size.y = shape.size()
	if size.y > 0:
		size.x	= shape[0].size()

func _swap(array: Array):
	var array_size = array.size()
	var half = array_size / 2
	for i in half:
		var y = array_size - 1 - i
		var buffer = array[i]
		array[i] = array[y]
		array[y] = buffer


func copy() -> ShapeData:
	return ShapeData.new(shape, base_color)

func add_color(color : Color) -> ShapeData:
	base_color = color
	return self

func add_symmetry(symmetry: bool) -> ShapeData:
	if symmetry:
		_swap(shape)
	return self

func add_rotation(rot: int) -> ShapeData:
	if rot & 1 == 1:
		_swap(shape)
		var old_shape = shape
		size = Vector2i(size.y, size.x)
		shape = []
		shape.resize(size.y)
		for y in size.y:
			var array = []
			array.resize(size.x)
			shape[y] = array
		for y in size.y:
			for x in size.x:
				shape[y][x] = old_shape[x][y]

	if rot & 2 == 2:
		_swap(shape)
		for y in size.y:
			_swap(shape[y])

	return self

func draw(position: Vector2i, centered_top_left: Vector2i, grid: Image, display: Image):
	for y in size.y:
		for x in size.x:
			if shape[y][x] != 0:
				var pixel_pos = position + Vector2i(x,y)
				var result_color = grid.get_pixelv(pixel_pos) + base_color
				var time = Time.get_unix_time_from_system() * SHAPE_FADE_SPEED
				var color = lerp(result_color, base_color, min(1, pingpong(time, SHAPE_FADE_SCALE)))
				display.set_pixelv(pixel_pos + centered_top_left, color)
