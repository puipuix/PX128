class_name MyImagePX

var _image : Image = Image.new()
var _scale : int
var _margin : int
var _used_rect : Rect2

func _get_pixel_rect(x: int, y: int) -> Rect2:
	return Rect2(_margin + x * _scale, _margin +  y * _scale, _scale, _scale)

func get_image() -> Image: return _image

func create(width: int, height: int, use_mipmaps: bool, format: int, scale: int, margin: int):
	_scale = scale
	_margin = margin
	_used_rect = Rect2(_margin, _margin, width * scale, height * scale)
	_image.create(width * scale + 2 * margin, height * scale + 2 * margin, use_mipmaps, format)
	print("create")

func fill(clear: Color, color: Color):
	_image.fill(clear)
	_image.fill_rect(_used_rect, color)

func get_pixel(x: int, y: int) -> Color:
	_image.lock()
	var color = _image.get_pixel(_margin + x * _scale, _margin +  y * _scale)
	_image.unlock()
	return color
	
func set_pixel(x: int, y: int, color: Color):
	_image.fill_rect(_get_pixel_rect(x, y), color)

func surround_pixel(x: int, y: int, color: Color, width: int):
	_image.lock()
	var fill_color = _image.get_pixel(_margin + x * _scale, _margin +  y * _scale)
	var rect = _get_pixel_rect(x, y)
	_image.fill_rect(rect.grow(width), color)
	_image.fill_rect(rect, fill_color)
	_image.unlock()

func get_unscaled_pixel(x: int, y: int) -> Color:
	_image.lock()
	var color = _image.get_pixel(_margin + x, _margin + y)
	_image.unlock()
	return color
	
func set_unscaled_pixel(x: int, y: int, color: Color):
	_image.lock()
	_image.set_pixel(_margin + x, _margin + y, color)
	_image.unlock()
	
func fill_unscaled_rect(x: int, y: int, width: int, height: int, color: Color):
	_image.fill_rect(Rect2(_margin + x, _margin + y, width, height), color)
