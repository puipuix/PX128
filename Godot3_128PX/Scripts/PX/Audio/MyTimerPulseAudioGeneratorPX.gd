extends MyPulseAudioGenerator

class_name MyTimerPulseAudioGeneratorPX

var min_pulse_frequency := 0.0
var max_pulse_frequency := 0.0
var pulse_frequency_curve := 1.0

func update_timer(time: float, time_left: float):
	if time > 0:
		pulse_frequency = lerp(min_pulse_frequency, max_pulse_frequency, pow(1 - time_left / time, pulse_frequency_curve))
	else:
		pulse_frequency = min_pulse_frequency
