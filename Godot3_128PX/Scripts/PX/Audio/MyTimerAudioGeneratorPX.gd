extends MyAudioGeneratorBase

class_name MyTimerAudioGeneratorPX

var low_frequency := 0.0
var high_frequency := 0.0

var timer_curve := 2.0
var refresh_curve := 1.0

var min_weight = 2.0
var max_weight = 4.0

var min_refresh_every := 0.5
var max_refresh_every := 2.0

var _refresh_time := 0.0
var _target_time_weight := 0.0

func update_timer(time: float, time_left: float):
	if time > 0:
		_target_time_weight = 1.0 - time_left / time
	else:
		_target_time_weight = 0.0

func force_refresh():
	_refresh_time = 0.0

func _process(delta):
	_refresh_time -= delta
	if _refresh_time < 0.0:
		var w = pow(_target_time_weight, refresh_curve)
		weight = lerp(min_weight, max_weight, w)
		_refresh_time = lerp(max_refresh_every, min_refresh_every, w)
		target_frequency = lerp(low_frequency, high_frequency, pow(_target_time_weight, timer_curve))
