extends MyAudioGeneratorBase

class_name MyGridAudioGeneratorPX

var low_frequency := 0.0
var high_frequency := 0.0
var curve := 1.0


func process_pixels(display: MyImagePX, size_x: int, size_y: int, center: int):
	var sum_values := 0.0
	var sum_weights := 0.0
	for y in size_y:
		for x in size_x:
			var color = display.get_pixel(x, y)
			var value = max(color.r, max(color.g, color.b))
			var weight : float = 1.0 - abs(x-center) / (size_x as float)
			sum_values += value * weight
			sum_weights += weight
	
	target_frequency = lerp(low_frequency, high_frequency, pow(sum_values / sum_weights, curve))
