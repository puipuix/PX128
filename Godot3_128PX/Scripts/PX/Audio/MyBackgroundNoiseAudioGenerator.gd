extends MyAudioGeneratorBase

class_name MyBackgroundNoiseAudioGenerator

var low_frequency := 0.0
var high_frequency := 0.0

var min_time := 0.0
var max_time := 0.0
var time_curve := 1.0

var grid: Array
var size_x: int
var size_y: int

var _time_left := 0.0
var freeze_time := false

func _process(delta):
	if not freeze_time:
		_time_left -= delta
	
	if _time_left < 0.0:
		_time_left = lerp(min_time, max_time, pow(randf(),time_curve))
	
		var x = randi() % size_x
		var y = randi() % size_y
		var color : Color = grid[y][x]
		var weight = (color.r + color.g + color.b) / 3.0
		
		target_frequency = lerp(low_frequency, high_frequency, weight)
