class_name MyNumberPX

const LINE_MARGIN = 8
const LINE_WIDTH = 4

const CHAR_WIDTH = 4
const CHAR_HEIGHT = 5
const SPACE_WIDTH = 1

const COLOR := Color8(64,64,64)

const DEBUG_CHARS = ".0123456789BiKMGTPE"

var _numbers = {
	" ": 0b1,
	".": 0b010_000_000_000_000_1,
	"0": 0b0110_1001_1001_1001_0110_0,
	"1": 0b0100_0100_0100_0110_0100_0,
	"2": 0b1111_0010_0100_1001_0110_0,
	"3": 0b0110_1001_0100_1001_0110_0,
	"4": 0b0100_0100_1111_0101_0100_0,
	"5": 0b0111_1000_0111_0001_1111_0,
	"6": 0b0110_1001_0111_0001_0110_0,
	"7": 0b0010_0010_0100_1000_1111_0,
	"8": 0b0110_1001_0110_1001_0110_0,
	"9": 0b0110_1000_1110_1001_0110_0,
	"B": 0b0111_1001_0111_1001_0111_0,
	"i": 0b010_010_010_000_010_1,
	"K": 0b1001_1001_0111_0101_1001_0,
	"M": 0b1001_1001_1001_1111_1001_0,
	"G": 0b0110_1001_1101_0001_0110_0,
	"T": 0b010_010_010_010_111_1,
	"P": 0b0001_0001_0111_1001_0111_0,
	"E": 0b1111_0001_0111_0001_1111_0,
}

func draw(number: int, size_x: int, pos_y: int, display: MyImagePX, alpha: float, scale: int) -> bool:
	var string = String.humanize_size(number)
	
	#scale = 1
	#string = DEBUG_CHARS
	
	var size = string.length() * CHAR_WIDTH * scale + (string.length() - 1) * SPACE_WIDTH * scale
	if size > size_x:
		return false
	
	var pos_x : int = (size_x - size) / 2
	
	display.get_image().lock()
	
	var x = pos_x
	for i in string.length():
		x += draw_char(string[i], x, pos_y, display, alpha, scale) * scale
		x += SPACE_WIDTH * scale
	
	draw_subline(size_x, pos_y, alpha, scale, display)
	
	display.get_image().unlock()
	
	return true

func draw_subline(size_x: int, pos_y: int, alpha: float, scale: int, display: MyImagePX):
	var draw_color = Color(COLOR)
	draw_color.a = alpha
	
	var x = LINE_MARGIN
	var y = pos_y + CHAR_HEIGHT * scale + LINE_MARGIN
	
	var color := display.get_unscaled_pixel(x, y).blend(draw_color)
	display.fill_unscaled_rect(x, y, size_x - 2 * LINE_MARGIN, LINE_WIDTH, color)
	
func draw_char(c: String, pos_x: int, pos_y: int, display: MyImagePX, alpha: float, scale: int) -> int:
	var draw_color = Color(COLOR)
	draw_color.a = alpha
	var number : int = _numbers.get(c, -1)
	if number < 0:
		push_warning("Unknow char '" + c + "'")
		return 1
	
	var p : int = 1
	var width = CHAR_WIDTH
	if number & p == p:
		width -= 1
	
	for j in CHAR_HEIGHT:
		for i in width:
			p *= 2
			if number & p == p:
				var x = pos_x + i * scale
				var y = pos_y + j * scale
				var color := display.get_unscaled_pixel(x, y).blend(draw_color)
				display.fill_unscaled_rect(x, y, scale, scale, color)
				
	return width
