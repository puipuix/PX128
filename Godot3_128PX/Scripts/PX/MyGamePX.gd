extends MyGameBase

class_name MyGamePX

const TUTORIAL_TIME := 0

enum MyScoreAnimStatePX {CLEARING, SHOWING, WAITING } 
const ANIM_CLEAR_STEP_TIME := 0.016
const ANIM_SCORE_SHOW_TIME := 2.0
const ANIM_CLEAR_CELL_STEP_TIME := 0.064

const SCORE_DISPLAY_STEP_VALUE := 8
const SCORE_DISPLAY_MAX_VALUE := GRID_SIZE_X * GRID_SIZE_Y * 255
const SCORE_BINARY_HEIGHT := 2

const AUDIO_MIN_REFRESH_RATE := 1102.5
const AUDIO_MAX_REFRESH_RATE := 11025.0
const AUDIO_BUFFER_LENGTH := 0.1
const AUDIO_DECREASE_QUALITY_AFTER_COUNT := 3
const AUDIO_INCREASE_QUALITY_AFTER_COUNT := 20
const AUDIO_NO_LAG_COUNT_MAX := 3
const AUDIO_LAG_THRESHOLD_MAX := AUDIO_BUFFER_LENGTH * 1000 * 0.75
const AUDIO_LAG_THRESHOLD_MIN := AUDIO_BUFFER_LENGTH * 1000 * 0.1
const AUDIO_INCREASE_QUALITY := 0.1
const AUDIO_DECREASE_QUALITY := 0.25

const LINE_SCORE := 8

const GAME_SIZE_X := 8
const GAME_SIZE_Y := 16
const BACKGROUND_COLOR := Color8(32,32,32)
const CLEAR_COLOR := Color8(24,24,24)

const DISPLAY_SCALE := 20
const SCORE_SCALE := DISPLAY_SCALE / MyNumberPX.CHAR_HEIGHT
var DISPLAY_MARGIN := 10
const DISPLAY_SIZE := Vector2(GAME_SIZE_X, GAME_SIZE_Y) * DISPLAY_SCALE

var SHAPE_SQUARE := MyShapePX.new([[1,1],[1,1]])
var SHAPE_LINE := MyShapePX.new([[1,1,1,1]])
var SHAPE_L := MyShapePX.new([[1,1,1],[0,0,1]])
var SHAPE_S := MyShapePX.new([[0,1],[1,1],[1,0]])
var SHAPE_T := MyShapePX.new([[0,1],[1,1],[0,1]])

var SOUND_BANK := {
	(MySounds.WAVE_SINE as int) : preload("res://Sounds/sine.wav"),
	(MySounds.WAVE_SQUARE as int) : preload("res://Sounds/square.wav"),
	(MySounds.WAVE_SMOOTH_SQUARE as int) : preload("res://Sounds/smooth_square.wav"),
	(MySounds.WAVE_TRIANGLE as int) : preload("res://Sounds/triangle.wav"),
	(MySounds.WAVE_SAW as int) : preload("res://Sounds/saw.wav"),
}

var _display := MyImagePX.new()
var _texture := ImageTexture.new()

var _current_anim = MyScoreAnimStatePX.CLEARING
var _anim_clear_step_timer := 0.0
var _anim_clear_to_clear := []
var _anim_clear_cleared := []

var _anim_clear_cells := []

var _anim_score_show_timer := 0.0
var _score_binary := MyBinaryNumberPX.new()
var _score_number := MyNumberPX.new()

var _audio_button_pressed := MyTimerPulseAudioGeneratorPX.new()
var _audio_button_released := MyTimerPulseAudioGeneratorPX.new()
var _audio_place_shape := MyPulseAudioGenerator.new()
var _audio_delete_shape := MyPulseAudioGenerator.new()
var _audio_grid := MyGridAudioGeneratorPX.new()
var _audio_background_noise := MyBackgroundNoiseAudioGenerator.new()
var _audio_give_up := MyVariableAudioGenerator.new()
var _audio_clear_cell := MyPulseAudioGenerator.new()
var _audio_timer := MyTimerAudioGeneratorPX.new()
var _audio_generators := [_audio_clear_cell, _audio_timer, _audio_give_up, _audio_delete_shape, _audio_background_noise, _audio_button_pressed, _audio_button_released, _audio_place_shape, _audio_grid]
var _audio_sum_weight := 0.0
var _audio_startup_volume := -0.25
var _process_grid_audio := false

func _ready():
	get_viewport().connect("size_changed", self, "_on_viewport_changed")
	_on_viewport_changed()
	
	_button_give_up.visible = false
	
	_audio_button_pressed.reaction_speed = Vector2(3000,80000)
	_audio_button_pressed.pulse_frequency = 200.0
	_audio_button_pressed.min_pulse_frequency = 200.0
	_audio_button_pressed.max_pulse_frequency = 1000.0
	_audio_button_pressed.pulse_frequency_curve = 4
	_audio_button_pressed.stream = SOUND_BANK[MySounds.WAVE_SINE]
	
	_audio_button_released.reaction_speed = Vector2(8000,8000)
	_audio_button_released.pulse_frequency = 250.0
	_audio_button_released.min_pulse_frequency = 150.0
	_audio_button_released.max_pulse_frequency = 350.0
	_audio_button_released.pulse_frequency_curve = 2
	_audio_button_released.stream = SOUND_BANK[MySounds.WAVE_SQUARE]
	
	_audio_place_shape.reaction_speed = Vector2(10000,3000)
	_audio_place_shape.pulse_frequency = 700.0
	_audio_place_shape.stream = SOUND_BANK[MySounds.WAVE_SINE]
	
	_audio_delete_shape.reaction_speed = Vector2(10000,3000)
	_audio_delete_shape.pulse_frequency = 700.0
	_audio_delete_shape.stream = SOUND_BANK[MySounds.WAVE_SAW]
	
	_audio_clear_cell.reaction_speed = Vector2(1000000,1000000)
	_audio_clear_cell.pulse_frequency = 30000.0
	_audio_clear_cell.stream = SOUND_BANK[MySounds.WAVE_SAW]
	
	_audio_grid.reaction_speed = Vector2(200,500)
	_audio_grid.low_frequency = 80.0
	_audio_grid.high_frequency = 180.0
	_audio_grid.target_frequency = _audio_grid.low_frequency
	_audio_grid._current_frequency = _audio_grid.low_frequency
	_audio_grid.stream = SOUND_BANK[MySounds.WAVE_TRIANGLE]
	_audio_grid.curve = 2.0
	
	_audio_timer.reaction_speed = Vector2(2000,5000)
	_audio_timer.low_frequency = 110.0
	_audio_timer.high_frequency = 220.0
	_audio_timer.refresh_curve = 1.0
	_audio_timer.timer_curve = 2.0
	_audio_timer.min_refresh_every = 0.5
	_audio_timer.max_refresh_every = 2.0
	_audio_timer.stream = SOUND_BANK[MySounds.WAVE_SINE]
	
	_audio_background_noise.reaction_speed = Vector2(500,500)
	_audio_background_noise.stream = SOUND_BANK[MySounds.WAVE_SINE]
	_audio_background_noise.low_frequency = 220.0
	_audio_background_noise.high_frequency = 330.0
	_audio_background_noise.min_time = 0.5
	_audio_background_noise.max_time = 3.0
	_audio_background_noise.time_curve = 2.0
	_audio_background_noise.grid = _grid
	_audio_background_noise.size_x = GRID_SIZE_X
	_audio_background_noise.size_y = GRID_SIZE_Y
	
	_audio_give_up.reaction_speed = Vector2(2000,2000)
	_audio_give_up.stream = SOUND_BANK[MySounds.WAVE_SAW]
	_audio_give_up.low_frequency = 0.0
	_audio_give_up.high_frequency = 500.0
	
	var audio_sum_weight = 10.0
	_audio_clear_cell.weight = 2.0 / audio_sum_weight
	_audio_give_up.weight = 2.0 / audio_sum_weight
	_audio_button_pressed.weight = 3.0 / audio_sum_weight
	_audio_button_released.weight = 3.0 / audio_sum_weight
	_audio_place_shape.weight = 4.0 / audio_sum_weight
	_audio_delete_shape.weight = 4.0 / audio_sum_weight
	_audio_grid.weight = 1.0 / audio_sum_weight
	_audio_timer.min_weight = 1.0 / audio_sum_weight
	_audio_timer.max_weight = 5.0 / audio_sum_weight
	_audio_background_noise.weight = 5.0 / audio_sum_weight
	
	_process_grid_audio = true
	for audio in _audio_generators:
		add_child(audio)

func _on_viewport_changed():
	var size = get_viewport_rect().size
	print("Viewport changed -> ", size)
	position = (size - DISPLAY_SIZE) * 0.5
	DISPLAY_MARGIN = max(position.x, position.y) as int
	_display.create(GAME_SIZE_X, GAME_SIZE_Y, false, Image.FORMAT_RGB8, DISPLAY_SCALE, DISPLAY_MARGIN)
	_texture.create_from_image(_display.get_image(), Texture.FLAG_REPEAT)
	
func _process(delta):
	update()
	if _audio_startup_volume != 1.0:
		_audio_startup_volume = move_toward(_audio_startup_volume, 1.0, delta)
		
	var i := _anim_clear_cells.size()
	while i > 0:
		i -= 1
		var data = _anim_clear_cells[i]
		data.time -= delta
		if data.time < 0.0:
			_anim_clear_cells.remove(i)
			_audio_clear_cell.on_pulse()

	if _process_grid_audio:
		_audio_grid.process_pixels(_display, GRID_SIZE_X, GRID_SIZE_Y, 0)
		
	var any_pressed := false
	var any_released := false
	for button in _buttons:
		any_pressed = any_pressed or button.just_pressed
		any_released = any_released or button.just_released
	
	if any_pressed:
		_audio_button_pressed.on_pulse()
	if any_released:
		_audio_button_released.on_pulse()
	
	_process_grid_audio = any_released
	_audio_background_noise.freeze_time = _current_state != MyGameState.GAME
	
	if _current_state == MyGameState.SCORE:
		_audio_timer.update_timer(0.0, 0.0)
		_audio_button_pressed.update_timer(0.0,0.0)
		_audio_button_released.update_timer(0.0,0.0)
		_audio_give_up.vary(0.0)
		if _current_anim == MyScoreAnimStatePX.CLEARING:
			_anim_clear_step_timer -= delta
		elif _current_anim == MyScoreAnimStatePX.SHOWING:
			_anim_score_show_timer += delta
	elif _current_state == MyGameState.GAME:
		_audio_timer.update_timer(_timer.time, _timer.time_left)
		_audio_button_pressed.update_timer(_timer.time, _timer.time_left)
		_audio_button_released.update_timer(_timer.time, _timer.time_left)
		_audio_give_up.vary(1.0 - _give_up_timer / GIVE_UP_TIME)
	
func _draw():
	_display.fill(CLEAR_COLOR, BACKGROUND_COLOR)
	
	if _current_state == MyGameState.GAME:
		_draw_game()
	elif _current_state == MyGameState.SCORE:
		_draw_score()
		
	_texture.set_data(_display.get_image())
	draw_texture(_texture, -Vector2.ONE * DISPLAY_MARGIN)


func _draw_game():
	_current_anim = MyScoreAnimStatePX.CLEARING
	
	for y in GRID_SIZE_Y:
		for x in GRID_SIZE_X:
			_display.set_pixel(x, y, _grid[y][x])
	
	for clearing_cell in _anim_clear_cells:
		_display.set_pixel(clearing_cell.x, clearing_cell.y, clearing_cell.color)
	
	var out_color := Ref.new()
	if _can_place_shape(out_color):
		_button_enter.custom_color = null 
	elif out_color.ref is Color:
		_button_enter.custom_color = out_color.ref

	_timer.draw(GRID_SIZE_Y, 0, GRID_SIZE_X, _display, GIVE_UP_TIME, _give_up_timer)
	
	_current_shape.surround(_current_shape_position_x, _current_shape_position_y, _display)
	_current_shape.draw(_current_shape_position_x, _current_shape_position_y, _grid, _display)

	for button in _buttons:
		button.draw(_display)

func _try_push_back_to_clear(pos: Vector2, dir: Vector2):
	var dest = pos + dir
	if dest.x < 0 or GRID_SIZE_X <= dest.x or dest.y < 0 or GRID_SIZE_Y <= dest.y:
		return
	
	if _anim_clear_to_clear.find(dest) != -1 or _anim_clear_cleared.find(dest) != -1:
		return
	
	_anim_clear_to_clear.push_back(dest)

func _draw_clear_anim():
	if _anim_clear_cleared.empty():
		_anim_clear_step_timer = 0.0
		for x in _current_shape.size_x:
			for y in _current_shape.size_y:
				if _current_shape.shape[y][x] != 0:
					_anim_clear_to_clear.push_back(Vector2(_current_shape_position_x + x, _current_shape_position_y + y))
	
	if _anim_clear_step_timer <= 0.0:
		_process_grid_audio = true
		_anim_clear_step_timer = ANIM_CLEAR_STEP_TIME
		var count := _anim_clear_to_clear.size()
		while count > 0:
			count -= 1
			var pos : Vector2 = _anim_clear_to_clear.pop_front()
			_anim_clear_cleared.append(pos)
			_try_push_back_to_clear(pos, Vector2.UP)
			_try_push_back_to_clear(pos, Vector2.DOWN)
			_try_push_back_to_clear(pos, Vector2.LEFT)
			_try_push_back_to_clear(pos, Vector2.RIGHT)
	
	for y in GRID_SIZE_Y:
		for x in GRID_SIZE_X:
			if _anim_clear_cleared.find(Vector2(x, y)) == -1:
				_display.set_pixel(x, y, _grid[y][x])
	
	if _anim_clear_to_clear.empty():
		_current_anim = MyScoreAnimStatePX.SHOWING
		_anim_clear_cleared.clear()

func _draw_score_value(score: int):
	score *= SCORE_DISPLAY_STEP_VALUE
	var g = min(score, SCORE_DISPLAY_MAX_VALUE)
	var r = clamp(score - SCORE_DISPLAY_MAX_VALUE, 0, SCORE_DISPLAY_MAX_VALUE)
	var b = clamp(score - 2 * SCORE_DISPLAY_MAX_VALUE, 0, SCORE_DISPLAY_MAX_VALUE)
	for y in GRID_SIZE_Y:
		for x in GRID_SIZE_X:
			var color = Color8(min(r, 255) as int, min(g, 255) as int, min(b, 255) as int)
			_display.set_pixel(x, y, color)
			r = max(0, r - 255)
			g = max(0, g - 255)
			b = max(0, b - 255)

func _draw_score_number(progress: float):
	if not _score_number.draw(_current_score, DISPLAY_SIZE.x as int, (GRID_SIZE_Y) * DISPLAY_SCALE + DISPLAY_SCALE / 2, _display, progress, SCORE_SCALE):
		_score_binary.draw(GRID_SIZE_X, SCORE_BINARY_HEIGHT, GRID_SIZE_Y, _display, progress, _current_score)

func _draw_show_anim():
	_process_grid_audio = true
	var progress = _anim_score_show_timer / ANIM_SCORE_SHOW_TIME
	_draw_score_value(_current_score * progress)
	_draw_score_number(progress)
	if _anim_score_show_timer > ANIM_SCORE_SHOW_TIME:
		_current_anim = MyScoreAnimStatePX.WAITING

func _draw_wait_anim():
	_draw_score_value(_current_score)
	_button_enter.draw(_display)
	_draw_score_number(1.0)

func _draw_score():
	_button_enter.custom_color = null 
	if _current_anim == MyScoreAnimStatePX.CLEARING:
		_draw_clear_anim()
	elif _current_anim == MyScoreAnimStatePX.SHOWING:
		_draw_show_anim()
	if _current_anim == MyScoreAnimStatePX.WAITING:
		_draw_wait_anim()

func _clear_cell(x: int, y: int, origin_x: int, origin_y: int):
	var data = {}
	data.x = x
	data.y = y
	data.time = max(abs(x - origin_x), abs(y - origin_y)) * ANIM_CLEAR_CELL_STEP_TIME
	data.color = _grid[y][x]
	_anim_clear_cells.append(data)
	
	._clear_cell(x, y, origin_x, origin_y)

func _place_shape():
	._place_shape()
	_audio_place_shape.on_pulse()
	_audio_timer.force_refresh()

func _delete_shape():
	._delete_shape()
	_audio_delete_shape.on_pulse()

func __generate_values() -> Array:
	return [MyShapePX.SHAPE_COLOR_RED, MyShapePX.SHAPE_COLOR_GREEN, MyShapePX.SHAPE_COLOR_BLUE]

func __generate_shapes() -> Array:
	return [SHAPE_SQUARE, SHAPE_LINE, SHAPE_L, SHAPE_S, SHAPE_T]

func __generate_timer() -> MyTimerBase:
	return MyTimerPX.new()

func __generate_button(button: int, key: int) -> MyButtonBase:
	var rect_start_x : int
	var rect_start_y : int
	var rect_size_x : int
	var rect_size_y : int
	if button == MyGameButton.ROTATE:
		rect_start_x = 6
		rect_start_y = 14
		rect_size_x = 1
		rect_size_y = 1
	elif button == MyGameButton.ENTER:
		rect_start_x = 3
		rect_start_y = 11
		rect_size_x = 2
		rect_size_y = 2
	elif button == MyGameButton.UP:
		rect_start_x = 3
		rect_start_y = 9
		rect_size_x = 2
		rect_size_y = 1
	elif button == MyGameButton.LEFT:
		rect_start_x = 1
		rect_start_y = 11
		rect_size_x = 1
		rect_size_y = 2
	elif button == MyGameButton.DOWN:
		rect_start_x = 3
		rect_start_y = 14
		rect_size_x = 2
		rect_size_y = 1
	elif button == MyGameButton.RIGHT:
		rect_start_x = 6
		rect_start_y = 11
		rect_size_x = 1
		rect_size_y = 2
	elif button == MyGameButton.DELETE:
		rect_start_x = 1
		rect_start_y = 14
		rect_size_x = 1
		rect_size_y = 1
	elif button == MyGameButton.GIVE_UP:
		rect_start_x = 0
		rect_start_y = 8
		rect_size_x = 8
		rect_size_y = 1
	
	return MyButtonPX.new(rect_start_x, rect_start_y, rect_size_x, rect_size_y, DISPLAY_SCALE, key)

func __get_default_value():
	return Color.black

func __get_score_for_value(value) -> int:
	return (value.r8 + value.g8 + value.b8) / MyShapePX.SHAPE_COLOR_VALUE * LINE_SCORE

func __get_score_for_shape(shape: MyShapeBase) -> int:
	return shape.size_x * shape.size_y

func __can_add_value(grid_value, shape_value) -> bool:
	var result := MyShapePX.get_result_color(grid_value, shape_value)
	return (result.r8 + result.g8 + result.b8) < MyShapePX.GRID_MAX_COLOR_VALUE * 3 # and result.g8 < MyShapePX.GRID_MAX_COLOR_VALUE and result.b8 < MyShapePX.GRID_MAX_COLOR_VALUE

func __get_added_value(grid_value, shape_value):
	return MyShapePX.get_result_color(grid_value, shape_value)

func _are_color_value_equals(a: int, b: int):
	return abs(a-b) < MyShapePX.SHAPE_COLOR_EPSILON

func __are_values_equals(value_a, value_b) -> bool:
	var r = _are_color_value_equals(value_a.r8, value_b.r8)
	var g = _are_color_value_equals(value_a.g8, value_b.g8)
	var b = _are_color_value_equals(value_a.b8, value_b.b8)
	return r and g and b

func __get_tutorial_time() -> int:
	return TUTORIAL_TIME

func __generate_tutorial() -> Array:
	return [
		# INIT
		MyTutorialOverrideTimerStep.new(self, 0),
		MyTutorialSetButtonLockedStep.new(self, true, _buttons),
		MyTutorialInitColorStepPX.new(self),
		
		# FORCE SHAPE
		MyTutorialSetShapeStep.new(self, SHAPE_SQUARE.copy().change_value(MyShapePX.SHAPE_COLOR_RED)),
		MyTutorialMoveShapeStep.new(self, (GRID_SIZE_X - SHAPE_SQUARE.size_x) / 2, (GRID_SIZE_X - SHAPE_SQUARE.size_y) / 2),

		# MOVE LEFT
		MyTutorialSetButtonHighlightStepPX.new(self, true, [_button_left]),
		MyTutorialSetButtonLockedStep.new(self, false, [_button_left]),
		MyTutorialButtonReleasedStep.new(self, _button_left),
		MyTutorialSetButtonLockedStep.new(self, true, [_button_left]),
		MyTutorialSetButtonHighlightStepPX.new(self, false, [_button_left]),
		
		# MOVE UP
		MyTutorialSetButtonHighlightStepPX.new(self, true, [_button_up]),
		MyTutorialSetButtonLockedStep.new(self, false, [_button_up]),
		MyTutorialButtonReleasedStep.new(self, _button_up),
		MyTutorialSetButtonLockedStep.new(self, true, [_button_up]),
		MyTutorialSetButtonHighlightStepPX.new(self, false, [_button_up]),
		
		# MOVE DOWN
		MyTutorialSetButtonHighlightStepPX.new(self, true, [_button_down]),
		MyTutorialSetButtonLockedStep.new(self, false, [_button_down]),
		MyTutorialButtonReleasedStep.new(self, _button_down),
		MyTutorialSetButtonLockedStep.new(self, true, [_button_down]),
		MyTutorialSetButtonHighlightStepPX.new(self, false, [_button_down]),

		# MOVE RIGHT 3
		MyTutorialSetButtonHighlightStepPX.new(self, true, [_button_right]),
		MyTutorialSetButtonLockedStep.new(self, false, [_button_right]),
		MyTutorialButtonReleasedStep.new(self, _button_right),
		MyTutorialButtonPressedStep.new(self, _button_right),
		MyTutorialButtonReleasedStep.new(self, _button_right),
		MyTutorialButtonPressedStep.new(self, _button_right),
		MyTutorialButtonReleasedStep.new(self, _button_right),
		MyTutorialSetButtonLockedStep.new(self, true, [_button_right]),
		MyTutorialSetButtonHighlightStepPX.new(self, false, [_button_right]),

		# PLACE FIRST SHAPE
		MyTutorialSetButtonHighlightStepPX.new(self, true, [_button_enter]),
		MyTutorialSetButtonLockedStep.new(self, false, [_button_enter]),
		MyTutorialButtonReleasedStep.new(self, _button_enter),
		MyTutorialSetButtonLockedStep.new(self, true, [_button_enter]),
		MyTutorialSetButtonHighlightStepPX.new(self, false, [_button_enter]),
		
		# FORCE SHAPE
		MyTutorialOverrideTimerStep.new(self, 0),
		MyTutorialSetShapeStep.new(self, SHAPE_S.copy().change_value(MyShapePX.SHAPE_COLOR_BLUE).change_rotation(1)),
		
		# DELETE
		MyTutorialSetButtonHighlightStepPX.new(self, true, [_button_delete]),
		MyTutorialSetButtonLockedStep.new(self, false, [_button_delete]),
		MyTutorialButtonReleasedStep.new(self, _button_delete),
		MyTutorialSetButtonLockedStep.new(self, true, [_button_delete]),
		MyTutorialSetButtonHighlightStepPX.new(self, false, [_button_delete]),
		
		# FORCE SHAPE
		MyTutorialOverrideTimerStep.new(self, 0),
		MyTutorialSetShapeStep.new(self, SHAPE_LINE.copy().change_value(MyShapePX.SHAPE_COLOR_GREEN)),
		
		# ROTATE
		MyTutorialSetButtonHighlightStepPX.new(self, true, [_button_rotate]),
		MyTutorialSetButtonLockedStep.new(self, false, [_button_rotate]),
		MyTutorialButtonReleasedStep.new(self, _button_rotate),
		MyTutorialSetButtonLockedStep.new(self, true, [_button_rotate]),
		MyTutorialSetButtonHighlightStepPX.new(self, false, [_button_rotate]),
		
		# PLACE SECOND SHAPE
		MyTutorialSetButtonHighlightStepPX.new(self, true, [_button_enter]),
		MyTutorialSetButtonLockedStep.new(self, false, [_button_enter]),
		MyTutorialButtonReleasedStep.new(self, _button_enter),
		MyTutorialSetButtonHighlightStepPX.new(self, false, [_button_enter]),
		
		# END
		MyTutorialSetButtonLockedStep.new(self, false, _buttons),
		MyTutorialOverrideDeleteCountDownStep.new(self, 0)
		]
