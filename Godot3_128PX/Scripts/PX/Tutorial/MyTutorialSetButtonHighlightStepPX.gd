extends MyTutorialStepBase

class_name MyTutorialSetButtonHighlightStepPX

var _state : bool
var _buttons : Array

func _init(game: Node2D, state : bool, buttons : Array).(game):
	_state = state
	_buttons = buttons

func __start(_game: Node2D):
	for button in _buttons:
		button.highlight = _state

func __is_complete(_game: Node2D) -> bool:
	return true
