extends MyTutorialStepBase

class_name MyTutorialInitColorStepPX

func _init(game: Node2D).(game):
	pass

func __start(game: Node2D):
	var x_start = (game.GRID_SIZE_X - game.SHAPE_SQUARE.size_x) / 2 + 2
	var y_exclude_start = (game.GRID_SIZE_Y - game.SHAPE_SQUARE.size_y) / 2
	for y in game.GRID_SIZE_Y:
		if y < y_exclude_start or y_exclude_start + game.SHAPE_SQUARE.size_y - 1 < y:
			for x in game.SHAPE_SQUARE.size_x:
				game._grid[y][x_start + x] = MyShapePX.SHAPE_COLOR_RED
	
	x_start = (game.GRID_SIZE_X - game.SHAPE_LINE.size_y) / 2 + 2 # y because shape should be rotated
	y_exclude_start = (game.GRID_SIZE_Y - game.SHAPE_LINE.size_x) / 2 # x because shape should be rotated
	for y in game.GRID_SIZE_Y:
		if y < y_exclude_start or y_exclude_start + game.SHAPE_LINE.size_x - 1 < y:
			for x in game.SHAPE_LINE.size_y:
				game._grid[y][x_start + x] += MyShapePX.SHAPE_COLOR_GREEN

func __is_complete(_game: Node2D) -> bool:
	return true
