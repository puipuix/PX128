extends MyTimerBase

class_name MyTimerPX

const TIMER_COLOR := Color.lightslategray
const MAX_TIME := 30.0
const MIN_TIME := 10.0
const SCORE_MULT := 0.001

func _draw(t: float, tl: float, y: int, x_min: int , x_max: int, display: MyImagePX):
	if t <= 0 || x_min == x_max:
		return
	
	var center = (x_min + x_max - 1) * 0.5
	var width = x_max - x_min
	var step = 2 * t / width
	for x in range(x_min, x_max):
		var distance = abs(x - center) + 0.5
		var x_time = step * distance
		if tl > x_time:
			display.set_pixel(x,y,TIMER_COLOR)
		else:
			var display_color = display.get_pixel(x,y)
			var weight = (tl - x_time + step) / step
			var color = lerp(display_color, TIMER_COLOR, max(0, weight))
			display.set_pixel(x,y,color)

func draw(y: int, x_min: int , x_max: int, display: MyImagePX, give_up_time: float, give_up_timer: float):
	if give_up_timer < give_up_time:
		_draw(give_up_time, give_up_timer, y, x_min, x_max, display)
	else:
		_draw(time, time_left, y, x_min, x_max, display)

func __score_to_time(score: int):
	return max(MIN_TIME, MAX_TIME / (1.0 + score * SCORE_MULT))
