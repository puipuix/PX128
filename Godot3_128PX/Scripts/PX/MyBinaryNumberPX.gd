class_name MyBinaryNumberPX

const BINARY_0_COLOR := Color8(48,48,48)
const BINARY_1_COLOR := Color8(64,64,64)

func draw(size_x: int, size_y: int, pos_y: int, display: MyImagePX, alpha: float, number: int):
	var v := 1
	for iy in size_y:
		for ix in size_x:
			var x = size_x - ix - 1
			var y = pos_y + size_y - iy - 1
			var color : Color
			if number & v == v:
				color = Color(BINARY_1_COLOR)
			else:
				color = Color(BINARY_0_COLOR)
			
			color.a = alpha
			color = display.get_pixel(x, y).blend(color)
			display.set_pixel(x,y,color)
			v = v << 1
