extends MyButtonBase

class_name MyButtonPX

const BUTTON_FADE_SPEED := 3.0
const BUTTON_FADE_SCALE := 1.0

const BUTTON_BASE_COLOR := Color.darkgray
const BUTTON_PRESSED_COLOR := Color.lightgray
const BUTTON_LOCKED_COLOR := Color.dimgray
const BUTTON_HIGHLIGHT_COLOR := Color.slategray
const BUTTON_HIGHLIGHT_SURROUND_COLOR := Color.darkred

var custom_color
var visible := true
var highlight := false

var rect_start_x : int
var rect_start_y : int
var rect_end_x : int
var rect_end_y : int
var scale : int

func _init(_rect_start_x : int, _rect_start_y : int, size_x : int, size_y : int, _scale: int, _key := KEY_UNKNOWN).(_key):
	rect_start_x = _rect_start_x
	rect_start_y = _rect_start_y
	rect_end_x = rect_start_x + size_x
	rect_end_y = rect_start_y + size_y
	key = _key
	scale = _scale

func __is_pressed(game: Node2D):
	var mouse_pos_x : int = (game.get_local_mouse_position().x / scale) as int
	var mouse_pos_y : int = (game.get_local_mouse_position().y / scale) as int
	var is_on = rect_start_x <= mouse_pos_x and mouse_pos_x < rect_end_x and rect_start_y <= mouse_pos_y and mouse_pos_y < rect_end_y
	return (is_on and Input.is_mouse_button_pressed(BUTTON_LEFT)) or Input.is_key_pressed(key)

func draw(display: MyImagePX):
	if !visible: return
	
	var color = BUTTON_BASE_COLOR
	if pressed:
		color = BUTTON_PRESSED_COLOR
	if locked:
		color = BUTTON_LOCKED_COLOR
	if custom_color is Color:
		color = custom_color

	if highlight:
		var time = OS.get_ticks_msec() * BUTTON_FADE_SPEED / 1000
		var weight = min(1, MyMath.smooth_pingpong(time, BUTTON_FADE_SCALE))
		color = lerp(color, BUTTON_HIGHLIGHT_COLOR, weight)
		
		for x in range(rect_start_x, rect_end_x):
			for y in range(rect_start_y, rect_end_y):
				display.surround_pixel(x, y, lerp(BUTTON_HIGHLIGHT_COLOR,BUTTON_HIGHLIGHT_SURROUND_COLOR, weight), 3)

	for x in range(rect_start_x, rect_end_x):
		for y in range(rect_start_y, rect_end_y):
			display.set_pixel(x, y, color)

