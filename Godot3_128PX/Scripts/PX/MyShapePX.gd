extends MyShapeBase

class_name MyShapePX

const SHAPE_SURROUND_COLOR := Color.silver# Color8(224,224,244)
const SHAPE_SURROUND_WIDTH := 1

const SHAPE_COLOR_VALUE := 128
const SHAPE_COLOR_OVERFLOW := SHAPE_COLOR_VALUE / 2
const SHAPE_COLOR_EPSILON := SHAPE_COLOR_OVERFLOW / 2
const SHAPE_COLOR_RED := Color8(SHAPE_COLOR_VALUE,0,0)
const SHAPE_COLOR_GREEN := Color8(0,SHAPE_COLOR_VALUE,0)
const SHAPE_COLOR_BLUE := Color8(0,0,SHAPE_COLOR_VALUE)

const GRID_MAX_COLOR_VALUE := 255 + SHAPE_COLOR_OVERFLOW
const GRID_MAX_SUM_VALUE := 255 * 3 + SHAPE_COLOR_OVERFLOW

const SHAPE_FADE_SPEED := 2.0
const SHAPE_FADE_SCALE := 1.5

func _init(_shape, _value = null).(_shape, _value):
	pass

func surround(position_x: int, position_y: int, display: MyImagePX):
	for y in size_y:
		for x in size_x:
			if shape[y][x] != 0:
				var grid_pos_x = position_x + x
				var grid_pos_y = position_y + y
				display.surround_pixel(grid_pos_x, grid_pos_y, SHAPE_SURROUND_COLOR, SHAPE_SURROUND_WIDTH)

func draw(position_x: int, position_y: int, grid: Array, display: MyImagePX):
	for y in size_y:
		for x in size_x:
			if shape[y][x] != 0:
				var grid_pos_x = position_x + x
				var grid_pos_y = position_y + y
				var result_color = get_result_color(grid[grid_pos_y][grid_pos_x], value)
				var time = OS.get_ticks_msec() * SHAPE_FADE_SPEED / 1000
				
				var color = lerp(result_color, value, min(1, MyMath.smooth_pingpong(time, SHAPE_FADE_SCALE)))
				display.set_pixel(grid_pos_x, grid_pos_y, color)

static func get_result_color(grid_value: Color, shape_value: Color) -> Color:
	var sum = grid_value + shape_value
	if (sum.r8 + sum.g8 + sum.b8) > GRID_MAX_SUM_VALUE:
		return Color.white * 2
	elif sum.r8 > GRID_MAX_COLOR_VALUE or sum.g8 > GRID_MAX_COLOR_VALUE or sum.b8 > GRID_MAX_COLOR_VALUE:
		return Color.white
	else:
		return sum
