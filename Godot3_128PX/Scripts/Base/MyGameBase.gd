extends Node2D

class_name MyGameBase

const GIVE_UP_TIME := 3.0
const DELETE_COUNTDOWN := 3
const ACTION_FREEZE_TIMER := 0.05
const GRID_SIZE_X := 8
const GRID_SIZE_Y := 8

enum MyGameState {GAME, SCORE}
enum MyGameButton {ROTATE, ENTER, UP, LEFT, DOWN, RIGHT, GIVE_UP, DELETE}

var _all_values : Array
var _all_shapes : Array
var _timer : MyTimerBase

var _button_rotate : MyButtonBase
var _button_enter : MyButtonBase
var _button_up : MyButtonBase
var _button_left : MyButtonBase
var _button_down : MyButtonBase
var _button_right : MyButtonBase
var _button_give_up : MyButtonBase
var _button_delete : MyButtonBase
var _buttons : Array
var _score_buttons : Array
var _game_buttons : Array

func __generate_values() -> Array:
	return []

func __generate_shapes() -> Array:
	return []

func __generate_timer() -> MyTimerBase:
	return null

func __generate_button(_button: int, _key: int) -> MyButtonBase:
	return null

func __get_default_value():
	return null

func __get_score_for_value(_value) -> int:
	return 0

func __get_score_for_shape(_shape: MyShapeBase) -> int:
	return 0

func __can_add_value(_grid_value, _shape_value) -> bool:
	return false

func __get_added_value(_grid_value, _shape_value):
	return null

func __generate_tutorial() -> Array:
	return []

func __are_values_equals(_value_a, _value_b) -> bool:
	return false

func __get_tutorial_time() -> int:
	return 0

var _current_score := 0
var _score_to_add := 0
var _current_shape : MyShapeBase
var _current_shape_position_x := 0
var _current_shape_position_y := 0
var _time_lock := false
var _grid : Array
var _tutorial_steps : Array
var _current_state : int
var _delete_countdown := 0
var _give_up_timer := 0.0 

func _ready():
	randomize()

	_all_values = __generate_values()
	_all_shapes = __generate_shapes()
	_timer = __generate_timer()

	_button_rotate = __generate_button(MyGameButton.ROTATE, KEY_R)
	_button_enter = __generate_button(MyGameButton.ENTER, KEY_SPACE)
	_button_up = __generate_button(MyGameButton.UP, KEY_Z)
	_button_left = __generate_button(MyGameButton.LEFT, KEY_Q)
	_button_down = __generate_button(MyGameButton.DOWN, KEY_S)
	_button_right = __generate_button(MyGameButton.RIGHT, KEY_D)
	_button_give_up = __generate_button(MyGameButton.GIVE_UP, KEY_G)
	_button_delete = __generate_button(MyGameButton.DELETE, KEY_ALT)
	_buttons = [_button_enter, _button_up, _button_left, _button_down, _button_right, _button_rotate, _button_give_up, _button_delete]
	_game_buttons = [_button_enter, _button_up, _button_left, _button_down, _button_right, _button_rotate, _button_give_up, _button_delete]
	_score_buttons = [_button_enter]
	
	_grid = []
	for y in GRID_SIZE_Y:
		var array = []
		for x in GRID_SIZE_X:
			array.append(__get_default_value())
		_grid.append(array)
	
	var need_tutorial = true
	var file = File.new()
	if file.file_exists("user://tutorial.dat"):
		file.open("user://tutorial.dat", File.READ)
		var time = file.get_64()
		file.close()
		need_tutorial = OS.get_unix_time() - time > __get_tutorial_time()
	
	if need_tutorial:
		_tutorial_steps = __generate_tutorial()
	else:
		_tutorial_steps = []
	
	_restart()

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
		return

	if _current_state == MyGameState.GAME:
		_process_game(delta)
	elif _current_state == MyGameState.SCORE:
		_process_score(delta)

func _process_score(_delta):
	for button in _buttons:
		button.update(self)
	
	if _button_enter.just_released:
		_restart()

func _process_game(delta):
	var had_step = _tutorial_steps.size() > 0
	while _tutorial_steps.size() > 0:
		var current_step : MyTutorialStepBase = _tutorial_steps[0]
		if not current_step.is_started():
			current_step.start()
		current_step.update(delta)
		if current_step.is_complete():
			_tutorial_steps.pop_front()
		else:
			break
	
	if had_step and _tutorial_steps.size() == 0:
		var file = File.new()
		file.open("user://tutorial.dat", File.WRITE)
		file.store_64(OS.get_unix_time())
		file.close()


	var freeze_timer := false
	for button in _buttons:
		button.update(self)
	
	var movement_x := 0
	var movement_y := 0
	if _button_down.just_released:
		movement_y += 1
		freeze_timer = true
	if _button_up.just_released:
		movement_y -= 1
		freeze_timer = true
	if _button_right.just_released:
		movement_x += 1
		freeze_timer = true
	if _button_left.just_released:
		movement_x -= 1
		freeze_timer = true
	
	if _button_rotate.just_released:
		freeze_timer = true
		var half_x = _current_shape.size_x / 2 # Recenter
		var half_y = _current_shape.size_y / 2
		_current_shape.change_rotation(1)
		_current_shape_position_x += half_x - 1 # Recenter
		_current_shape_position_y += half_y - 1

	if not _time_lock:
		if freeze_timer:
			_timer.freeze(ACTION_FREEZE_TIMER)
		_timer.update(delta)
		if _timer.is_ended():
			if _can_place_shape():
				_place_and_change_shape()
			else:
				_lose()
		elif _button_enter.just_released:
			if _can_place_shape():
				_place_and_change_shape()
		elif _button_delete.just_released:
			if _delete_countdown <= 0:
				_delete_shape()
	
	if _button_give_up.pressed:
		_give_up_timer -= delta
		if _give_up_timer <= 0.0:
			_lose()
	else:
		_give_up_timer = GIVE_UP_TIME
	_current_shape_position_x = clamp(_current_shape_position_x + movement_x, 0, GRID_SIZE_X - _current_shape.size_x) as int
	_current_shape_position_y = clamp(_current_shape_position_y + movement_y, 0, GRID_SIZE_Y - _current_shape.size_y) as int

func _reset_grid():
	for y in GRID_SIZE_Y:
		for x in GRID_SIZE_X:
			_grid[y][x] = __get_default_value()

func _lose():
	_current_state = MyGameState.SCORE
	
	set_button_locked(true, _game_buttons)
	set_button_locked(false, _score_buttons)

func _restart():
	_reset_grid()
	_pick_random_shape()
	_timer.reset(0)
	_current_shape_position_x = (GRID_SIZE_X - _current_shape.size_x) / 2
	_current_shape_position_y = (GRID_SIZE_Y - _current_shape.size_y) / 2
	_current_score = 0
	_delete_countdown = 0
	_button_delete.locked = false
	_give_up_timer = GIVE_UP_TIME
	_current_state = MyGameState.GAME
	
	set_button_locked(true, _score_buttons)
	set_button_locked(false, _game_buttons)

func _delete_shape():
	_change_shape()
	_button_delete.locked = true
	_delete_countdown = DELETE_COUNTDOWN

func _place_and_change_shape():
	_place_shape()
	_try_clear(_current_shape_position_x, _current_shape_position_y, _current_shape_position_x + _current_shape.size_x, _current_shape_position_y + _current_shape.size_y)
	if _score_to_add > 0:
		_current_score += _score_to_add
		print("Add ", _score_to_add, " points -> ", _current_score)
		
		_score_to_add = 0
		print("New time: ", _timer.score_to_time(_current_score))
	
	_change_shape()
	_timer.reset(_current_score)

func _change_shape():
	var half_x = _current_shape.size_x / 2 # Recenter
	var half_y = _current_shape.size_y / 2
	_pick_random_shape()
	_current_shape_position_x += half_x - 1 # Recenter
	_current_shape_position_y += half_y - 1

func _check_values(i_start: int, i_end: int, j_start: int, j_end: int, get_value: FuncRef) -> Array:
	var i_to_clear = []
	for i in range(i_start, i_end):
		var same_values = true
		var test_value = get_value.call_func(i, 0)
		for j in range(j_start, j_end):
			if not __are_values_equals(get_value.call_func(i, j), test_value):
				same_values = false
				break
		if same_values:
			i_to_clear.append(i)
	return i_to_clear

func _get_grid_value_xy(x, y):
	return _grid[y][x]
func _get_grid_value_yx(y, x):
	return _grid[y][x]

func _try_clear(start_x: int, start_y: int, end_x: int, end_y: int):
	var x_to_clear = _check_values(start_x, end_x, 0, GRID_SIZE_Y, funcref(self, "_get_grid_value_xy"))
	var y_to_clear = _check_values(start_y, end_y, 0, GRID_SIZE_X, funcref(self, "_get_grid_value_yx"))
	var add_score := 0
	var score_mult := 0
	for x in x_to_clear:
		var grid_value = _grid[0][x]
		add_score += __get_score_for_value(grid_value)
		score_mult += 1
		for y in GRID_SIZE_Y:
			_clear_cell(x,y, x, clamp(y, start_y, end_y - 1) as int)
	for y in y_to_clear:
		var grid_value = _grid[y][0]
		add_score += __get_score_for_value(grid_value)
		score_mult += 1
		for x in GRID_SIZE_X:
			_clear_cell(x,y, clamp(x, start_x, end_x - 1) as int, y)
	
	_score_to_add += add_score * score_mult
	if add_score > 0:
		print("Cleared score: ", add_score, "(x", score_mult, ")")

func _clear_cell(x: int, y: int, _origin_x: int, _origin_y: int):
	_grid[y][x] = __get_default_value()

func _can_place_shape(out_value: Ref = null) -> bool:
	for y in _current_shape.size_y:
		for x in _current_shape.size_x:
			if _current_shape.shape[y][x] != 0:
				var grid_pos_x = _current_shape_position_x + x
				var grid_pos_y = _current_shape_position_y + y
				var grid_value = _grid[grid_pos_y][grid_pos_x]
				if not __can_add_value(grid_value, _current_shape.value):
					if out_value != null:
						out_value.ref = grid_value
					return false
	return true

func _place_shape():
	if _delete_countdown > 0:
		_delete_countdown -= 1
		if _delete_countdown == 0:
			_button_delete.locked = false
	
	var add_score = __get_score_for_shape(_current_shape)
	_score_to_add += add_score
	print("Place score: ", add_score)
	for y in _current_shape.size_y:
		for x in _current_shape.size_x:
			if _current_shape.shape[y][x] != 0:
				var grid_pos_x = _current_shape_position_x + x
				var grid_pos_y = _current_shape_position_y + y
				var grid_value = _grid[grid_pos_y][grid_pos_x]
				_grid[grid_pos_y][grid_pos_x] = __get_added_value(grid_value, _current_shape.value)

func _pick_random_shape():
	var shape = _all_shapes[randi() % _all_shapes.size()]
	var value = _all_values[randi() % _all_values.size()]
	_current_shape = shape.copy().change_value(value).change_symmetry(randf() < 0.5).change_rotation(randi() % 4)

func set_button_locked(lock: bool, buttons: Array):
	for button in buttons:
		button.locked = lock
