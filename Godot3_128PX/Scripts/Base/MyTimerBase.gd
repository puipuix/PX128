class_name MyTimerBase

var freeze_time := 0.0
var time := 0.0
var time_left := 0.0

func score_to_time(score: int) -> float:
	return __score_to_time(score)

func reset(score: int):
	time = score_to_time(score)
	time_left = time

func freeze(freeze: float):
	freeze_time = max(freeze, freeze_time)

func update(delta: float):
	if freeze_time > 0:
		freeze_time -= delta
	else:
		time_left = max(0, time_left - delta)
	
func is_ended() -> bool: return time != time_left and time_left <= 0.0

func __score_to_time(_score: int) -> float:
	return 0.0
