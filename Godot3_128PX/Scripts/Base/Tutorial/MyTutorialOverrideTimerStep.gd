extends MyTutorialStepBase

class_name MyTutorialOverrideTimerStep

var _time

func _init(game: Node2D, time: float).(game):
	_time = time

func __start(game: Node2D):
	game._timer.time = _time
	game._timer.time_left = _time

func __is_complete(_game: Node2D) -> bool:
	return true
