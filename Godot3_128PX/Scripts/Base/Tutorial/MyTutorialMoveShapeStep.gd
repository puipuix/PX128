extends MyTutorialStepBase

class_name MyTutorialMoveShapeStep

var _at_position_x : int
var _at_position_y : int

func _init(game: Node2D, at_position_x: int, at_position_y: int).(game):
	_at_position_x = at_position_x
	_at_position_y = at_position_y

func __start(game: Node2D):
	game._current_shape_position_x = _at_position_x
	game._current_shape_position_y = _at_position_y

func __is_complete(_game: Node2D) -> bool:
	return true
