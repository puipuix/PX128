extends MyTutorialStepBase

class_name MyTutorialOverrideDeleteCountDownStep

var _countdown

func _init(game: Node2D, countdown: float).(game):
	_countdown = countdown

func __start(game: Node2D):
	game._delete_countdown = _countdown
	if _countdown == 0:
		game._button_delete.locked = false

func __is_complete(_game: Node2D) -> bool:
	return true
