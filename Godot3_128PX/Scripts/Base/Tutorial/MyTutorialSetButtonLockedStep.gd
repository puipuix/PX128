extends MyTutorialStepBase

class_name MyTutorialSetButtonLockedStep

var _state : bool
var _buttons : Array

func _init(game: Node2D, state : bool, buttons : Array).(game):
	_state = state
	_buttons = buttons

func __start(_game: Node2D):
	_game.set_button_locked(_state, _buttons)

func __is_complete(_game: Node2D) -> bool:
	return true
