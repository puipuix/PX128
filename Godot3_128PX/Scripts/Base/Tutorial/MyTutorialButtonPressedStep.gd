extends MyTutorialStepBase

class_name MyTutorialButtonPressedStep

var _button

func _init(game: Node2D, button: MyButtonBase).(game):
	_button = button

func __is_complete(_game: Node2D) -> bool:
	return _button.just_pressed
