extends MyTutorialStepBase

class_name MyTutorialSetShapeStep

var _shape : MyShapeBase

func _init(game: Node2D, shape : MyShapeBase).(game):
	_shape = shape

func __start(game: Node2D):
	game._current_shape = _shape.copy()

func __is_complete(_game: Node2D) -> bool:
	return true
