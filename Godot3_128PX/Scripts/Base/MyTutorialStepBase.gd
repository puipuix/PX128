class_name MyTutorialStepBase

var _game
var _started := false

func _init(game):
	_game = game

func start():
	_started = true
	__start(_game)

func update(delta: float):
	__update(_game, delta)

func is_complete() -> bool:
	return _started and __is_complete(_game)

func is_started() -> bool:
	return _started

# warning-ignore:shadowed_variable
func __start(_game: Node2D):
	pass

# warning-ignore:shadowed_variable
func __update(_game: Node2D, _delta: float):
	pass

# warning-ignore:shadowed_variable
func __is_complete(_game: Node2D) -> bool:
	return false
