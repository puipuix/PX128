class_name MyShapeBase

var value
var size_x := 0
var size_y := 0
var shape := []

func _init(_shape : Array, _value = null):
	value = _value
	shape = _shape
	size_y = shape.size()
	if size_y > 0:
		size_x	= shape[0].size()

func _swap(array: Array):
	var array_size = array.size()
	var half = array_size / 2
	for i in half:
		var y = array_size - 1 - i
		var buffer = array[i]
		array[i] = array[y]
		array[y] = buffer


func copy() -> MyShapeBase:
	return get_script().new(shape, value)

func change_value(_value) -> MyShapeBase:
	value = _value
	return self

func change_symmetry(symmetry: bool) -> MyShapeBase:
	if symmetry:
		_swap(shape)
	return self

func change_rotation(rot: int) -> MyShapeBase:
	if rot & 1 == 1:
		_swap(shape)
		var old_shape = shape
		var size_tmp = size_y
		size_y = size_x
		size_x = size_tmp
		shape = []
		shape.resize(size_y)
		for y in size_y:
			var array = []
			array.resize(size_x)
			shape[y] = array
		for y in size_y:
			for x in size_x:
				shape[y][x] = old_shape[x][y]

	if rot & 2 == 2:
		_swap(shape)
		for y in size_y:
			_swap(shape[y])

	return self
