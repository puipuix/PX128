class_name MyButtonBase

var locked := false
var pressed : bool
var just_released : bool
var just_pressed : bool
var key : int

func _init(_key := KEY_UNKNOWN):
	key = _key

func update(game: Node2D):
	var is_pressed = __is_pressed(game) and not locked
	just_pressed = not pressed and is_pressed and not locked
	just_released = pressed and not is_pressed and not locked
	pressed = is_pressed and not locked

func __is_pressed(_game: Node2D):
	return Input.is_key_pressed(key)
