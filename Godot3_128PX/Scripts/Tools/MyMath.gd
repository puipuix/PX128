class_name MyMath

static func smooth_pingpong(value: float, max_value: float) -> float:
	if max_value == 0.0:
		return 0.0
	
	return (sin((value / max_value - 0.5) * PI) * 0.5 + 0.5) * max_value

