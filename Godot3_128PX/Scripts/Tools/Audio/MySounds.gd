class_name MySounds

enum {
	WAVE_SINE,
	WAVE_SQUARE,
	WAVE_SMOOTH_SQUARE,
	WAVE_TRIANGLE,
	WAVE_PULSE,
	WAVE_SAW,
}

static func compute_wave(wave_phase: float, wave_shape) -> float:
	if wave_shape == WAVE_SINE:
		return sin(wave_phase * TAU)
	elif wave_shape == WAVE_SQUARE:
		if wave_phase < 0.5:
			return -1.0
		else:
			return 1.0
	elif wave_shape == WAVE_TRIANGLE:
		if wave_phase < 0.5:
			return -1.0 + wave_phase * 4
		else:
			return 1.0 - (wave_phase - 0.5) * 4
	elif wave_shape == WAVE_PULSE:
		if wave_phase < 0.8:
			return -1.0
		else:
			return 1.0
	elif wave_shape == WAVE_SAW:
		return wave_phase * 2.0 - 1.0
	else:
		return 0.0
