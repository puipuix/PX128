extends MyAudioGeneratorBase

class_name MySirenAudioGenerator

var low_frequency := 0.0
var high_frequency := 0.0
var siren_shape = MySounds.WAVE_SINE
var siren_frequency := 0.0

var _siren_phase := 0.0

func _process(delta):
	target_frequency = lerp(low_frequency, high_frequency, MySounds.compute_wave(_siren_phase, siren_shape) * 0.5 + 0.5)
	_siren_phase = fmod(_siren_phase + siren_frequency * delta, 1.0)
