extends MyAudioGeneratorBase

class_name MyPulseAudioGenerator

var pulse_frequency := 0.0
var idle_frequency := 0.0

func on_pulse():
	target_frequency = pulse_frequency

func _process(_delta):
	if _current_frequency >= target_frequency:
		target_frequency = idle_frequency
