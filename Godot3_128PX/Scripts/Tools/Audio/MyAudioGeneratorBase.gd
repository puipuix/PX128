extends AudioStreamPlayer

class_name MyAudioGeneratorBase

const AUDIO_DEFAULT_FREQUENCY := 1000.0

var reaction_speed := Vector2.ONE * 1000
var target_frequency := 0.0
var weight := 0.0

var _current_frequency := 0.0

func _process(delta):
	if target_frequency > _current_frequency:
		if reaction_speed.x < 0:
			_current_frequency = target_frequency
		else:
			_current_frequency = move_toward(_current_frequency, target_frequency, reaction_speed.x * delta)
	elif target_frequency < _current_frequency:
		if reaction_speed.y < 0:
			_current_frequency = target_frequency
		else:
			_current_frequency = move_toward(_current_frequency, target_frequency, reaction_speed.y * delta)
	
	if _current_frequency == 0 or weight == 0:
		stop()
	else:
		volume_db = linear2db(weight)
		pitch_scale = _current_frequency / AUDIO_DEFAULT_FREQUENCY
		if not playing:
			play()
	
