extends MyAudioGeneratorBase

class_name MyVariableAudioGenerator

var low_frequency := 0.0
var high_frequency := 0.0
var curve := 1.0

func vary(coef: float):
	target_frequency = lerp(low_frequency, high_frequency, pow(coef,curve))
